import os
import re
import json
import numpy as np
import matplotlib.pyplot as plt
import datetime
import utm
import netCDF4
from shapely.geometry import LineString
import subprocess
import gpxpy
import utm
import rasterio
import contextily as ctx
from geopy import distance
from geopy.distance import great_circle
import matplotlib.colors as colors


import cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature
coast = cfeature.GSHHSFeature(scale="full")
from shapely.geometry import shape
#from shapely.geometry import Polygon
from matplotlib.patches import Polygon
from matplotlib.font_manager import FontProperties
import matplotlib.patheffects as path_effects
from matplotlib.collections import PatchCollection

import matplotlib as mpl
#mpl.style.use("./hiking.mplstyle")


from matplotlib.font_manager import FontProperties
fa_dir = r"/home/ctroupin/.fonts/"
fp1 = FontProperties(fname=os.path.join(fa_dir, "Font Awesome 6 Free-Solid-900.otf"))
fawesome = fp1

fontfile = "/home/ctroupin/.fonts/D-DIN.ttf"
myfont = FontProperties(fname=fontfile)

apikey = os.environ["THUNDERKEY"]

thunder = ctx.providers.Thunderforest.Outdoors
thunder["apikey"] = apikey


CartoDB_Positron = {
    'url': 'https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png',
    'attribution': '© OpenStreetMap contributors CARTO',
    'maxZoom': 19
}

# Global definitions
data_crs = ccrs.PlateCarree()
main_crs = ccrs.Mercator()
maingreen = "#138101"   # forest
secondgreen = "#3CC47C" # electric

# Define bounding box
iccoords = (-18.5, -13.25, 27.49, 29.505)
gccoords = (-15.86, -15.35, 27.7, 28.2)
lpcoords = (-18.025, -17.70, 28.4385, 28.8591)
tnfcoords = (-17.0, -16.09, 27.97, 28.62)
ehcoords = (-18.22, -17.84, 27.6, 27.88)
lgcoords = (-17.38, -17.06, 28., 28.23)

# Files and directories
figdir = "../figures/"
firefigdir = os.path.join(figdir, "fires/")
datadir = "../Data/"
GCcoastfile = os.path.join(datadir, "Coastline/GCcontour.geojson")

class Island(object):
    def __init__(self, bbox=None, proj=None, contour=None):
        """
        bbox = bounding box (4-element tuple)
        proj = cartopy CCRS
        contour = coordinates of the coastline,
        - contour[0] contains longitudes,
        - contour[1] contains latitudes, both as list of lists
        """
        if bbox is not None:
            self.bbox = bbox
            self.proj = ccrs.Mercator(central_longitude=0.5 * (self.bbox[0] + self.bbox[1]),
            min_latitude=self.bbox[2], max_latitude=self.bbox[3],
            globe=None, latitude_true_scale=0.5 * (self.bbox[2] + self.bbox[3]))

    def get_coastline(self, datafile):
        """Read the coastline as a list of lists of coordinates from a geoJSON file
        """
        self.contour = ([], [])
        with open(datafile, "r") as df:
            data = json.load(df)
            coords = data['features'][0]['geometry']['coordinates']
            for contour in coords:
                lon, lat = [], []
                c0 = contour[0]
                for j in range(0, len(c0)):
                    lon.append(c0[j][0])
                    lat.append(c0[j][1])

                self.contour[0].append(lon)
                self.contour[1].append(lat)

    def get_island_polygon(self, jsonfile, codnut='ES705'):
        # Parse the JSON file
        with open(jsonfile, "r") as f:
            datam = json.load(f)

        # Loop on municipalities
        for municipios in datam["features"]:
            if municipios['properties']['CODNUT3'] == codnut:
                name = municipios['properties']['NAMEUNIT']
                coords = municipios['geometry']['coordinates']
                for c1 in coords:
                    for c2 in c1:
                        poly = Polygon(c2)
                        self.polygon = self.polygon.union(poly)

class Precipitation(object):
    """Precipitations"""
    def __init__(self, lon=[], lat=[], field=[], altitude=[], name=[]):
        self.lon = lon
        self.lat = lat
        self.field = field
        self.altitude = altitude
        self.name = name

    def read_ncfile(self, datafile):
        """Read the interpolated precipitation field as obtained by `DIVAnd`

        Note: the field is now called "heat" but that could/should change
        quickly. In order not to deal with the variable name, an attribute
        such as the _standard name_ will be added.
        """
        with netCDF4.Dataset(datafile, 'r') as nc:
            precip = nc.variables["heat"][:]
            self.lon = nc.variables["lon"][:]
            self.lat = nc.variables["lat"][:]
            self.field = nc.variables["heat"][:]

    def read_precipitation_insitu(self, datafile):
        """Read the in situ precipitation (annual mean) using the data file
        from Consejo Insular del Agua de Gran Canaria (`pluviometria.cv`)

        The coordinates have to be converted to WGS84, this is done with the
        `utm` module.
        """

        separator = ','
        lons = []
        lats = []
        with open(datafile) as csvfile:
            labelline = csvfile.readline().rstrip()
            labellist = labelline.split(",")
            
            # Get colomn indices
            lonindex = labellist.index("X-UTM")
            latindex = labellist.index("Y-UTM")
            fieldindex = labellist.index("Media Anual (mm)")
            altindex = labellist.index("Media Anual (mm)")
            nameindex = labellist.index("Nombre")
            
            for lines in csvfile:
                linesplit = lines.rstrip().split(separator)
                lons.append(float(linesplit[lonindex]))
                lats.append(float(linesplit[latindex]))
                self.field.append(float(linesplit[fieldindex]))
                self.name.append(linesplit[nameindex])
                self.altitude.append(float(linesplit[altindex]))
            
            self.lat, self.lon = utm.to_latlon(np.array(lons), np.array(lats), 28, "U")


class Topography(object):

    def __init__(self, lon=None, lat=None, extent=None, image=None):
        self.lon = lon
        self.lat = lat
        self.image = image
        self.extent = extent

    def read_diva_results(self, datafile):
        """Read the output from a DIVA 2D analysis"""

        with netCDF4.Dataset(datafile) as nc:
            self.lon = nc.variables["x"][:]
            self.lat = nc.variables["y"][:]
            self.image = nc.variables["analyzed_field"][:]
            self.extent = (self.lon[0], self.lon[-1], self.lat[0], self.lat[-1])

    def read_geotiff(self, datafile):
        with rasterio.open(datafile) as r:
            image = r.read()
            self.image = np.transpose(image, [1,2,0])
            if len(self.image.shape) == 3:
                self.image = self.image[:,:,0]


            proj = r.crs
            extent = r.bounds
            trans = r.transform
            x = np.arange(0, r.width)
            y = np.arange(0, r.height)
            xx, yy = np.meshgrid(x, y)

            self.lon = trans[0] * xx + trans[1] * yy + trans[2]
            self.lat = trans[3] * xx + trans[4] * yy + trans[5]
            self.extent = (extent.left, extent.right, extent.bottom, extent.top)

    def read_geotiff_domain(self, datafile, domain=None):
        """Read the geotiff from datafile and subset over the region
        defined by `domain`
        ```
        """
        self.read_geotiff(datafile)

        if domain is not None:
            dlon = abs(self.lon[1] - self.lon[0])
            dlat = abs(self.lat[1] - self.lat[0])

            # Subset domains
            goodlat = np.where( (self.lat >= domain[2] - dlat) & (self.lat <= domain[3] + dlat))[0]
            goodlon = np.where( (self.lon >= domain[0] - dlon) & (self.lon <= domain[1] + dlon))[0]

            self.image = self.image[goodlat,:]
            self.image = self.image[:,goodlon]

            self.lon = self.lon[goodlon]
            self.lat = self.lat[goodlat]

        self.extent = (self.lon[0], self.lon[-1], self.lat[0], self.lat[-1])

    def read_netcdf_gebco(self, datafile, domain=None):
        with netCDF4.Dataset(datafile, "r") as nc:
            lon = nc.variables["lon"][:]
            lat = nc.variables["lat"][:]
            arr = nc.variables["elevation"][:]

        if domain is None:
            self.lon = lon
            self.lat = lat
            self.image = arr
        else:
            # Subset domains
            goodlon = np.where( (lon >= domain[0] - dlon) & (lon <= domain[1] + dlon))[0]
            goodlat = np.where( (lat >= domain[2] - dlat) & (lat <= domain[3] + dlat))[0]

            arr_domain = arr[goodlat,:]
            arr_domain = arr_domain[:,goodlon]

            self.lon = lon[goodlon]
            self.lat = lat[goodlat]
            self.image = arr_domain


        self.extent = (self.lon[0], self.lon[-1], self.lat[0], self.lat[-1])

class Fire(object):
    def __init__(self, lon=None, lat=None, dates=None, brightness=None):
        """
        Start with an empty list
        """
        if lon is None:
            self.lon = np.array([])
        if lat is None:
            self.lat = np.array([])
        if dates is None:
            self.dates = np.array([])
        if brightness is None:
            self.brightness = np.array([])

    def read_from_json(self, datafile):
        with open(datafile, "r") as df:
            datafire = json.load(df)
        lon = []
        lat = []
        brightness = []
        dates = []
        for records in datafire:
            lon.append(records["longitude"])
            lat.append(records["latitude"])
            brightness.append(records["brightness"])
            date = records["acq_date"]
            hour = records["acq_time"]
            dates.append(datetime.datetime.strptime(date, '%Y-%m-%d'))

        self.lon = np.array(lon)
        self.lat = np.array(lat)
        self.brightness = np.array(brightness)
        self.dates = np.array(dates)

class Visible(object):

    def __init__(self, lon=None, lat=None, proj=None, extent=None, image=None):
        self.lon = lon
        self.lat = lat
        self.proj = proj
        self.image = image
        self.extent = extent

    def read_geotiff(self, imagefile):
        """
        ```python
        read_geotiff(imagefile)
        ```
        Read an image and compute the coordinates from a geoTIFF file.

        Input: imagefile
        -----
        """
        with rasterio.open(imagefile) as r:
            image = r.read()
            self.image = np.transpose(image, [1,2,0])
            
            self.proj = r.crs
            extent = r.bounds
            trans = r.transform
            x = np.arange(0, r.width)
            y = np.arange(0, r.height)
            xx, yy = np.meshgrid(x, y)

            self.lon = trans[0] * xx + trans[1] * yy + trans[2]
            self.lat = trans[3] * xx + trans[4] * yy + trans[5]
            self.extent = (extent.left, extent.right, extent.bottom, extent.top)

    def add_to_plot(self, ax, proj, **kwargs):
        """
        ```python
        add_to_plot(ax, myproj)
        ```
        Add the geoTIFF image to the plot.

        Inputs:
        ------
        ax: a figure ax object
        myproj: a cartopy projection
        """
        ax.imshow(self.image, origin='upper', extent=self.extent,
                  transform=proj, **kwargs)

def plot_coastline(ax, coastfile, **kwargs):
    """Plot the coastline read from the geoJSON file `coastfile`
    """

    with open(coastfile, "r") as df:
        data = json.load(df)

    coords = data['features'][0]['geometry']['coordinates']
    for ic, contour in enumerate(coords):
        lonc, latc = [], []
        # print(ic)
        c0 = contour[0]
        for j in range(0, len(c0)):
            lonc.append(c0[j][0])
            latc.append(c0[j][1])
        ax.plot(lonc, latc, transform=data_crs, **kwargs)

def get_bbox(filename, r=0.05):
    """
    get_bbox(filename)

    Return a 4-element array [latmin, lonmin, latmax, latmax], delimiting the bounding box around the
    positions stored in file `filename`.

    """
    lon, lat, ele = read_gpx_lonlat(filename)
    lon = np.array(lon)
    lat = np.array(lat)
    deltalon = lon.max() - lon.min()
    deltalat = lat.max() - lat.min()
    r = 0.05
    bbox = [lat.min() - r * deltalat, lon.min() - r * deltalon,
        lat.max() + r * deltalat, lon.max() + r * deltalon]
    return bbox

def read_gpx_lonlat(filename):
    lon, lat, elevation = [], [], []
    with open(filename) as f:
        datagpx = f.read()
        ff = datagpx.split('</trkpt>')
        for lines in ff:
            match = re.search('<trkpt lat="([-0-9\.]+)" lon="([-0-9\.]+)">', lines)
            if match:
                lat.append(float(match.group(1)))
                lon.append(float(match.group(2)))
                matchele = re.search("<ele>([-0-9\.]+)</ele>", lines)
                if matchele:
                    elevation.append(float(matchele.group(1)))

    return lon, lat, elevation

def plot_province(provincefile, ax, color="g"):
    """Plot the contours of the province stored in the geoJSON file
    `provincefile` and fill them in the specified color.
    """
    # Read the data from the file
    with open(provincefile, "r") as f:
        data = json.load(f)

    patches = []
    # Eastern islands
    coords = data['features'][0]['geometry']['coordinates']
    for contour in coords:
        lon, lat = [], []
        c0 = contour[0]
        for j in range(0, len(c0)):
            lon.append(c0[j][0])
            lat.append(c0[j][1])
        ax.plot(lon, lat, 'k-', linewidth=.1, transform=data_crs)

        polygon = Polygon(list(zip(lon, lat)), True)
        patches.append(polygon)
    p = PatchCollection(patches, alpha=.9, transform=data_crs)
    p.set_facecolors(color)
    ax.add_collection(p)

def add_province(ax, datafile, thecolor):
    with open(datafile, "r") as f:
        data = json.load(f)
    patches = []
    # Eastern islands
    coords = data['features'][0]['geometry']['coordinates']
    for contour in coords:
        lon, lat = [], []
        c0 = contour[0]
        print(len(c0))
        for j in range(0, len(c0)):
            lon.append(c0[j][0])
            lat.append(c0[j][1])
        ax.plot(lon, lat, 'k-', linewidth=.1, transform=ccrs.PlateCarree())

        polygon = Polygon(list(zip(lon, lat)), True)
        patches.append(polygon)
    p = PatchCollection(patches, alpha=.9, transform=ccrs.PlateCarree())
    p.set_facecolors(thecolor)
    ax.add_collection(p)

def enlarge_fig(ax):
    """Enlarge the plot extent to have a square domain centered on the track
    """
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()
    dx = xlims[1] - xlims[0]
    dy = ylims[1] - ylims[0]

    if dx > dy:
        delta = dx - dy
        ylim2 = [ylims[0] - delta/2, ylims[1] + delta/2]
        xlim2 = xlims
        ax.set_ylim(ylim2)
        print(ylims, ylim2)
    else:
        delta = dy - dx
        xlim2 = [xlims[0] - delta/2, xlims[1] + delta/2]
        ylim2 = ylims
        ax.set_xlim(xlim2)

    print(f"Domain: [{xlims}, {xlim2}, {ylims}, {ylim2}")


def scale_bar(ax, myproj, length=None, location=(0.5, 0.05), linewidth=3, fs=12):
    """
        scale_bar(ax, length=None, location=(0.5, 0.05), linewidth=3)

    Add a map scale on the figure defined by `ax` at location `location`

    ax is the axes to draw the scalebar on.
    length is the length of the scalebar in km.
    location is center of the scalebar in axis coordinates.
    (ie. 0.5 is the middle of the plot)
    linewidth is the thickness of the scalebar.
    """
    #Get the limits of the axis in lat long
    llx0, llx1, lly0, lly1 = ax.get_extent(ccrs.PlateCarree())
    #llx0, llx1 = ax.get_xlim()
    #lly0, lly1 = ax.get_ylim()
    #Make tmc horizontally centred on the middle of the map,
    #vertically at scale bar location
    sbllx = (llx1 + llx0) / 2
    sblly = lly0 + (lly1 - lly0) * location[1]
    #Get the extent of the plotted area in coordinates in metres
    x0, x1, y0, y1 = ax.get_extent(myproj)
    #Turn the specified scalebar location into coordinates in metres
    sbx = x0 + (x1 - x0) * location[0]
    sby = y0 + (y1 - y0) * location[1]

    #Calculate a scale bar length if none has been given
    #(Theres probably a more pythonic way of rounding the number but this works)
    if not length:
        length = (x1 - x0) / 5000 #in km
        ndim = int(np.floor(np.log10(length))) #number of digits in number
        length = round(length, -ndim) #round to 1sf
        #Returns numbers starting with the list
        def scale_number(x):
            if str(x)[0] in ['1', '2', '5']: return int(x)
            else: return scale_number(x - 10 ** ndim)
        length = scale_number(length)

    #Generate the x coordinate for the ends of the scalebar
    bar_xs = [sbx - length * 500, sbx + length * 500]
    #Plot the scalebar
    ax.plot(bar_xs, [sby, sby], transform=myproj, color='k', linewidth=linewidth)

    #Plot the scalebar label
    tt = ax.text(sbx, sby, str(length) + ' km', transform=myproj, fontsize=fs,
            horizontalalignment='center', verticalalignment='bottom')
    tt.set_path_effects([path_effects.PathPatchEffect(edgecolor='white', linewidth=1)])
    ax.text(sbx, sby, str(length) + ' km', transform=myproj, fontsize=fs,
            horizontalalignment='center', verticalalignment='bottom')


class Track:
    """
    GPX track storing coordinates
    """

    def __init__(self, lon=None, lat=None, ele=None, time=None):
        """
        Start with an empty list
        """
        if lon is None:
            self.lon = np.array([])
        if lat is None:
            self.lat = np.array([])
        if ele is None:
            self.ele = np.array([])
        if time is None:
            self.time = np.array([])

    def get_coords(self, gpxfile):
        """
        Read the coordinates and the time from a GPX file and return lists of
        longitude, latitude, elevation and time
        """
        if os.path.exists(gpxfile):
            with open(gpxfile, 'r', encoding="ISO-8859-1") as gf:
                gpx = gpxpy.parse(gf)
                for track in gpx.tracks:
                    for segment in track.segments:
                        for point in segment.points:
                            self.lon = np.append(self.lon, point.longitude)
                            self.lat = np.append(self.lat, point.latitude)
                            self.ele = np.append(self.ele, point.elevation)
                            self.time = np.append(self.time, point.time)

    def compute_dist(self):
        """
        Compute the distance between consecutive points using the Great Circle distance formula.
        Return a list of distance in kilometers. The total distance is the last element of the list.
        """
        npoints = len(self.lon)
        self.dist = np.zeros(npoints)
        td = 0
        for i in range(0, npoints-1):
            d = great_circle((self.lat[i], self.lon[i]), (self.lat[i+1], self.lon[i+1])).km
            td += d
            self.dist[i+1] = td

    def get_max_ele(self):
        """
        Compute the maximal elevation of the current track
        """
        e = np.array(self.ele)
        elemax = e.max()
        nmax = np.argmax(self.ele)
        return elemax, nmax

    def get_positive_ascent(self):
        """Compute positive and negative elevations
        (should be the same but are not always because of errors)
        """
        # Ensure we don't have None elements
        goodele = np.where(self.ele)[0]
        # Element wise difference
        diffele = self.ele[goodele[1:]] - self.ele[goodele[0:-1]]
        # Sum up positive and negative values
        elepos = np.sum(diffele[diffele > 0])
        eleneg = np.sum(diffele[diffele < 0])

        return elepos, eleneg

    def get_index_km(self):
        """
        Get the indices of the coordinates that correspond to km 1, km 2, ... km N
        """
        self.compute_dist()
        ind = []
        for dd in np.arange(1, np.round(self.dist[-1])):
            ind.append(np.argmin(abs(self.dist - dd)))

        return ind

    def scatter(self):
        plt.scatter(self.lon, self.lat, c=self.ele, s=5, edgecolor=None)
        plt.show()

    def enlarge_fig(self, dd=0.1):
        """Enlarge the plot extent to have a square domain centered on the track
        """
        lonmin = self.lon.min()
        lonmax = self.lon.max()
        latmin = self.lat.min()
        latmax = self.lat.max()
        dx = lonmax - lonmin
        dy = latmax - latmin

        if dx > dy:
            delta = dx - dy
            latmin = latmin - delta/2
            latmax = latmax + delta/2
            D = dx
        else:
            delta = dy - dx
            lonmin = lonmin - delta/2
            lonmax = lonmax + delta/2
            D = dy

        return lonmin - D * dd, lonmax + D * dd, latmin - D * dd, latmax + D * dd

    def make_minimap(self, figname=None):
        """
            make_minimap(lon, lat, figname)

        Create a mini-map of the island with the tracks plotted on it.
        """

        fig = plt.figure()
        ax = plt.subplot(111, projection=main_crs)
        ax.plot(self.lon, self.lat, transform=data_crs, linewidth=1, color="#008000")

        GC = Island()
        GC.get_coastline(GCcoastfile)

        for llon, llat in zip(GC.contour[0], GC.contour[1]):
            ax.plot(llon, llat, linewidth=.5, color="k", transform=data_crs)

        # Add municipios
        munifile = os.path.join(datadir, "municipiosGC.json")
        municipios = Municipios()
        municipios.get_from_json(munifile)
        municipios.add_to_plot(ax, color=".75", linewidth=.25)

        ax.spines['geo'].set_color("w")

        if figname is not None:
            plt.savefig(figname)
        else:
            plt.show()
        plt.close()


    def make_profile(self, figname=None):
        elemax, nmax = self.get_max_ele()
        self.compute_dist()

        fig = plt.figure(figsize=(10,3))
        ax = plt.subplot(111)
        plt.plot(self.dist, self.ele, color="#008000")
        plt.plot(self.dist[nmax], elemax, "*",
                 markersize=15, color="#008000", markeredgecolor="k")
        plt.text(self.dist[nmax], elemax, "    {} m".format(int(round(elemax))), ha='left')
        #plt.xlabel("Distance (km)", fontsize=14)
        #plt.ylabel("Altitude\n(m)", fontsize=14, rotation=0, ha='right')
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        plt.tick_params(axis='both', which='major', labelsize=14)
        plt.xlim(xmin = 0.)

        xticks, xlabels = plt.xticks()
        yticks, ylabels = plt.yticks()

        xticklabels = [str(int(xt)) for xt in xticks]
        xticklabels[-2] = xticklabels[-2] + ' km'
        yticklabels = ['{} m'.format(int(yt)) for yt in yticks]

        # ax.set_xticks(xticks)
        ax.set_xticklabels(xticklabels)
        ax.set_yticklabels(yticklabels)

        plt.grid(linewidth=.25)

        if figname is not None:
            plt.savefig(figname, transparent=False, dpi=300,
            edgecolor='white', bbox_inches='tight', pad_inches=0)
        else:
            plt.show()
        plt.close()

    def add_track(self, ax, outproj='epsg:3857', inproj='epsg:4326', color="#008000"):
        xxt, yyt = transform(inproj, outproj, self.lat, self.lon)
        ax.plot(xxt, yyt, color="white", linewidth=8, alpha=.5)
        ax.plot(xxt, yyt, color=color, linestyle="--", linewidth=2)

    def add_track_plain(self, ax, outproj='epsg:3857', inproj='epsg:4326', color="#008000"):
        xxt, yyt = transform(inproj, outproj, self.lat, self.lon)
        ax.plot(xxt, yyt, color=color, linestyle="-", linewidth=1)

    def add_km_markers(self, ax, color=maingreen, icon="\uf276"):

        # Extract at kilometers
        ind = self.get_index_km()
        londist = np.take(self.lon, ind)
        latdist = np.take(self.lat, ind)

        num = 0
        for llon, llat in zip(londist, latdist):
            num += 1
            ttt = ax.text(llon, llat, str(num) + "\n" + icon,
                          fontproperties=fp1, fontsize=14,
                          transform=data_crs, ha="center", va="bottom", color=color)
            ttt.set_path_effects([path_effects.Stroke(linewidth=2, foreground='white'),
                       path_effects.Normal()])


class Municipios:
    """Store limits of municipalities and their name
    """
    def __init__(self, lon=None, lat=None, name=None):
        if lon is None:
            self.lon = []
        if lat is None:
            self.lat = []
        if name is None:
            self.name = []

    def get_from_json(self, jsonfile):
        """Read from a json file
        and return lists of lists for longitude and latitude

        """

        if os.path.exists(jsonfile):
            with open(jsonfile, "r") as f:
                datam = json.load(f)
                for municipios in datam["features"]:

                    name = municipios['properties']['NAMEUNIT']
                    coords = municipios['geometry']['coordinates']
                    lona, lata = [], []
                    for c1 in coords:
                        lonm, latm = [], []
                        for c2 in c1:
                            for c3 in c2:
                                lonm.append(c3[0])
                                latm.append(c3[1])
                        lona.append(lonm)
                        lata.append(latm)
                    # Append the list corresponding to a municipality
                    # to the full list
                    self.lon.append(lona)
                    self.lat.append(lata)
                    if len(self.name) == 0 or self.name[-1] != name:
                        self.name.append(name)
                        
    def get_from_json_lp(self, jsonfile):
        """Read from a json file
        and return lists of lists for longitude and latitude

        """

        if os.path.exists(jsonfile):
            with open(jsonfile, "r") as f:
                datam = json.load(f)
                for municipios in datam["features"]:

                    name = municipios['properties']['municipio']
                    coords = municipios['geometry']['coordinates']
                    lona, lata = [], []
                    for c1 in coords:
                        lonm, latm = [], []
                        for c2 in c1:
                            for c3 in c2:
                                lonm.append(c3[0])
                                latm.append(c3[1])
                        lona.append(lonm)
                        lata.append(latm)
                    # Append the list corresponding to a municipality
                    # to the full list
                    self.lon.append(lona)
                    self.lat.append(lata)
                    if len(self.name) == 0 or self.name[-1] != name:
                        self.name.append(name)

    def add_to_plot(self, ax, **kwargs):
        """
        Add the municipalities limits to the plot
        """
        for lona, lata in zip(self.lon, self.lat):
            for lonm, latm in zip(lona, lata):
                ax.plot(lonm, latm, transform=data_crs,
                        **kwargs)


class Presas:
    """
    Store coordinates and capacity
    """
    def __init__(self, lon=[], lat=[], capacity=[]):
        self.lon = lon
        self.lat = lat
        self.capacity = capacity

    def get_from_file(self, filename):
        """
        Read the coordinates and the capacity from a KML file
        """
        if os.path.exists(filename):
            with open(filename, "r") as f:
                for lines in f:
                    match = re.search('<coordinates>([-0-9\.]+),([-0-9\.]+),([-0-9\.]+) </coordinates>', lines)
                    if match:
                        self.lon.append(float(match.group(1)))
                        self.lat.append(float(match.group(2)))
                    matchcap = re.search('SimpleData name="Capacidad">([0-9,.]{2,12}),[0-9]* [m³]*[ \*]*</SimpleData>', lines)
                    if matchcap:
                        # print(matchcap.group(1))
                        self.capacity.append(float(matchcap.group(1).replace('.','')))
                        # <SimpleData name="Capacidad">4.800.000,00 m³</SimpleData>

    def get_color_size(self, limits, colorlist):
        """
        Create lists of colors and marker size to be used in the plot
        """
        colors = []
        sizes = []
        ncolors = len(limits)

        # Loop on all the capacities
        for cap in self.capacity:
            # Loop on limit values defining intervals
            col = "k"
            size = 2
            for ii in range(0, ncolors):
                if cap > limits[ii]:
                    col = colorlist[ii]
                    size = 5
            colors.append(col)
            sizes.append(size)

        return colors, sizes

    def add_to_plot(self, ax, colors, sizes, **kwargs):
        """
        Scatter plot of the reservoirs with a color and a sizes
        depending on their capacity

        colors and size are lists with the same size as the coordinates
        """

        if isinstance(colors, (list,)):
            if len(colors) == len(self.lat):
                i = 0
                for lon, lat in zip(self.lon, self.lat):
                    ax.plot(lon, lat, "o",
                           markersize=sizes[i], color=colors[i],
                           transform=data_crs,
                           **kwargs)
                    i += 1
        elif isinstance(colors, (str,)):
            ax.plot(self.lon, self.lat, "o",
                   markersize=sizes, color=colors,
                   transform=data_crs,
                   **kwargs)


    def add_legend(self, ax, lims, colorlist):
        """
        Prepare legend
        we plot all the symbols at the location of the largest reservoir
        yeahhh, not very clever
        """
        pmax = np.argmax(np.array(self.capacity))
        lonleg, latleg = self.lon[pmax], self.lat[pmax]

        ax.plot(lonleg, latleg, "o", markersize=2, color="k",
               label="0$-$10", transform=data_crs)

        for i, cc in enumerate(colorlist[:-1]):
            labeltext = "$-$".join((str(lims[i]//1000), str(lims[i+1]//1000)))
            ax.plot(lonleg, latleg, "o", markersize=5, color=cc, markeredgecolor="k",
                   label=labeltext, transform=data_crs)
        ax.plot(lonleg, latleg, "o", markersize=5, color=colorlist[-1], markeredgecolor="k",
               label="$>$".join(("", str(lims[-1]//1000))), transform=data_crs)
        plt.legend(title="Capacité en milliers de m$^{3}$", fontsize=14)

def rect_from_bound(xmin, xmax, ymin, ymax):
    """Returns list of (x,y)'s for a rectangle"""
    xs = [xmax, xmin, xmin, xmax, xmax]
    ys = [ymax, ymax, ymin, ymin, ymax]
    return [(x, y) for x, y in zip(xs, ys)]


def get_extsquare(ext):
    """Return a square extent by adding borders to the current extent"""
    dlon = ext[1] - ext[0]
    dlat = ext[3] - ext[2]
    delta = abs(dlon - dlat)
    extsquare = [0, 0, 0, 0]
    if dlon > dlat:
        extsquare[2] = ext[2] - delta / 2
        extsquare[3] = ext[3] + delta / 2
        extsquare[0] = ext[0]
        extsquare[1] = ext[1]
    else:
        extsquare[0] = ext[0] - delta / 2
        extsquare[1] = ext[1] + delta / 2
        extsquare[3] = ext[3]
        extsquare[2] = ext[2]
        
    return extsquare

def get_bbox_json(datafile: str): 
    with open(datafile) as df:
        data = json.loads(df.read())
    coords = data["features"][0]["geometry"]["coordinates"][0]
    lonmin = coords[0][0]
    latmin = coords[0][1]
    lonmax = coords[2][0]
    latmax = coords[2][1]
    return lonmin, lonmax, latmin, latmax

def load_fonts():
    fontdir = "../fonts/"
    fontawesome = FontProperties(fname=os.path.join(fontdir, "Font Awesome 6 Free-Solid-900.otf"))
    fontbold = FontProperties(fname=os.path.join(fontdir, "Barlow-Bold.ttf"))
    fontreg = FontProperties(fname=os.path.join(fontdir, "Barlow-Regular.ttf"))
    
    return fontawesome, fontreg, fontbold

def get_delta(track):
    deltalon = track.lon.max() - track.lon.min()
    deltalat = track.lat.max() - track.lat.min()
    deltamin = np.min(np.array(deltalon, deltalat))
    return deltamin * 0.025

def get_basemap(provider, zoomlevel, domain, basemapfile=None):
    """
    Extract the basemap from the provider and the zoom level; 
    the region of interest is specified in the variable `domain`
    which stores [lonmin, lonmax, latmin, latmax]

    # Example of usage
    ```python
    if not(os.path.exists(backgroundfile)):
        background, backextend = get_basemap(provider, zoomlevel, domain, backgroundfile)
    else:
        vis = hikinggc.Visible()
        vis.read_geotiff(backgroundfile)
        background = vis.image
        backextend = vis.extent
    ```
    """

    if basemapfile is None:
        basemapfile = f"basemap_{provider.name}_{zoomlevel}"
        
    background, backextend = ctx.bounds2raster(domain[0], domain[2], domain[1], domain[3], 
                                               ll=True, path=basemapfile,
                                               source=provider, zoom=zoomlevel)
        
    return background, backextend

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap





