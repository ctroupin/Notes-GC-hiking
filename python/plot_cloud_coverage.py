import hikinggc
from importlib import reload
reload(hikinggc)

import os
import geojson
import json
import numpy as np
import matplotlib.pyplot as plt

import cartopy
import cartopy.crs as ccrs
import cartopy.mpl.gridliner as gridliner

import logging
import cmocean
from matplotlib.colors import LightSource
from osgeo import gdal
from osgeo import osr


coastfile1 = os.path.join(hikinggc.datadir, "ProvinciaLasPalmas.geojson")
coastfile2 = os.path.join(hikinggc.datadir, "ProvinciaSantaCruz.geojson")
datafile = os.path.join(hikinggc.datadir, "Rain/MODAL2_M_CLD_FR_2021-07-01_rgb_3600x1800.TIFF")
datafile2 = os.path.join(hikinggc.datadir, "MODCF_meanannual.tif")
topofile = os.path.join(hikinggc.datadir, '136_MDT25_GC/136_MDT25_GC_proj.tif')
topography = hikinggc.Topography()
topography.read_geotiff(topofile)
topography.image = np.ma.masked_less_equal(topography.image, 0.)


# In[3]:

print("light source")
# Shading
ls = LightSource(azdeg=315, altdeg=45)
rgb = ls.shade(topography.image, cmap=plt.cm.gray, blend_mode='soft',
               vert_exag=0.05)


# In[4]:


cloud = hikinggc.Topography()
cloud.read_geotiff_domain(datafile2, hikinggc.iccoords)


# In[5]:


# Mask
cloud.image = np.ma.masked_greater(cloud.image, 20000., copy=True)


# In[6]:


CI = hikinggc.Island(hikinggc.iccoords)


# In[7]:





# In[ ]:


fig = plt.figure()
ax = plt.subplot(111, projection=CI.proj)
print("adding cloud")
ax.pcolormesh(cloud.lon, cloud.lat, cloud.image, cmap=plt.cm.Blues_r,
          transform=hikinggc.data_crs, zorder=3)
print("adding topo")
ax.imshow(rgb, origin='upper', extent=topography.extent,
          transform=hikinggc.data_crs, zorder=4, alpha=.15)

#plot_coastline(ax, coastfile1)
#plot_coastline(ax, coastfile2)
print("setting extent")
ax.set_extent(CI.coordinates)

print("saving fig")
plt.savefig(os.path.join(hikinggc.figdir, "clouds_topo"))
# plt.show()
plt.close()
