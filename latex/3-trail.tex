\input{randoLayout.tex}
%-----------------------------------------------------------------------------------

\makeindex

\begin{document}
\pagestyle{empty}


\input{titlepage.tex}

\cleardoublepage

\tableofcontents
\addtocontents{toc}{~\hfill\textbf{Page}\par}


\chapter{Trail running sur les chemins}
\AddToShipoutPicture*{\BackgroundIm{Photos/trailChapter.jpg}}
\thispagestyle{empty}
\newpage
%------------------------------------------

Toutes les informations et les routes de randonnées présentées dans les chapitres précédents peuvent aussi être utilisées pour la pratique du trail, avec une large gamme de distances et de difficultés si on combine plusieurs itinéraires. Il faudra simplement être prudent et éviter les sentiers les plus populaires dans les heures de pointe, notamment les weekends, afin d'éviter les moments d'affluence de promeneurs.

Tout comme de nombreux endroits dans le monde, les Canaries ont assisté au développement de la popularité du trail, d'autant plus que l'Espagne compte des coureurs professionnels de référance, tels que Kylian Jornet ou Luis Alberto Hernando. Les Canaries ne sont pas en reste avec Cristofer Clemente, coureur originaire de l'île de La Gomera\index{Gomera, La} ou Efrén Segundo de Gran Canaria. Il faut reconnaître que le terrain d’entraînement dont disposent les athlètes canariens est exceptionnel: depuis pratiquement n'importe quel village, ils ont accès à des côtes en continu avec un dénivelé positif supérieur aux 1000 mètres. À la différence des Alpes ou des Pyrénées, les conditions météorologiques (Section~\ref{sec:meteo}, page~\pageref{sec:meteo}) sont idéales quasiment toute l'année, permettant ainsi les entraînements en short et t-shirt la plupart du temps.

La Fédération Canarienne de Montagne (FECAMON, \url{http://www.fecamon.es}) régit les activités de trail (assurances, compétitions, clubs), et une des fédérations sportives qui compte le plus d'affiliés. Chaque village possède son, voire ses clubs de course de montagnes ou de trail.

La variété du territoire et son relief abrupt, combinés à une météo agréable, font que certaines courses organisées dans l'archipel ont aujourd'hui une réputation qui a dépassé les frontières: la \textit{Transvulcanania}\index{Transvulcania} (73~km, La Palma\index{Palma, La}; \url{https://transvulcania.es}) organisée depuis 2009, la \textit{Haria Extreme} (95~km, Lanzarote\index{Lanzarote}; \url{https://www.hariaextreme.com}) ou encore la \textit{Transgrancanaria} (125~km, \url{https://www.transgrancanaria.net}). Participer à l'une de ces épreuves constituent une expérience d'exception qui permet de découvrir une île d'une manière différente, et en relativement peu de temps, le tout accompagné par un public nombreux et passionné.

Tout comme pour la partie consacrée aux randonnées, nous n'indiquerons pas ici de niveau de difficulté car nous considérons que c'est une information qui peut être subjective et dépendre de variables commes les condition météorologiques, l'état du terrain ou encore la forme du coureur. En particulier, on sera attentif aux périodes de \textit{calima} (Section~\ref{sec:calima}, page~\pageref{sec:calima}) sont peu propices aux longues sorties.

\section{La pratique du trail aux Canaries}
%-------------------------------------------

\subsection{Particularités}

L'origine volcanique de l'île est responsable de nombreux traits orographiques que sont les \textit{calderas} et autres \textit{barrancos}, dont on a parlé page \pageref{sec:terminologie}). Aussi bien en voiture sur la route qu'à pied sur les sentiers, on se trouve face à des pentes dont on n'a pas toujours l'habitude dans certains pays d'Europe. Ces rapides changements d'altitude peuvent d'ailleurs se succéder en à peine quelques heures ou kilomètres.

Les brusques variations de conditions météorologiques (température, humidité, vent) sur de très courtes distances ou intervalles de temps consistuent aussi un élément à ne pas oublier. Il suffit parfois de franchir un col pour passer d'un temps sec et ensoleillé à un temps venteux et humide. Lors des traversées (Section~\ref{sec:traversee}, page \pageref{sec:traversee}), on commence à proximité du niveau de la mer pour arriver à près de 2000~m d'altitude en quelques heures, ce qui peut donner lieu à une différence de température de l'ordre de 20 degrés celsius. Si à cela on ajoute l'effet du vent, la sensation de froid peut être encore plus accentuée. Comme dit l'adage, "un traileur averti en vaut deux", et prévoir des vêtements en accord avec le parcours est un minimum pour garder un certain confort lors de longues sorties.

\mysidebox[title={Température ressentie.}]{%
\includegraphics[width=.6\textwidth]{./figures/Tressentie.png}
}{%
Un indice appelé "température ressentie" a été créé pour rendre compte de la sensation de froid en fonction de la vitesse du vent, bien que ce ne soit pas une température à proprement parler. 
}

\begin{anecdote}
On se trouvait sur le parking du Pico de las Nieves\index{Pico de las Nieves} (1940 m d'altitude) pour profiter du coucher de soleil et prendre quelques photos. Curieusement il y avait de nombreuses voitures à côté de la notre, mais personne n'en sortait pendant plus de 2 minutes à cause de la sensation de froid provoquée par le vent du nord était forte. En discutant entre nous, on était d'accord pour dire qu'il devait y avoir 5$^{\circ}$C. En réalité, il y avait environ 15$^{\circ}$C!
\includegraphics[width=\textwidth]{atardecerFrio.jpg}
\end{anecdote}

\subsection{Quand les problèmes surviennent\ldots}

Bien qu'une part d'improvisation permet de bonnes surprises, il faut aussi savoir comment réagir en cas de pépins. On conseille très souvent de ne pas sortir seul. Si ça ne peut être évité, il est alors recommandé d'informer une personne de contact du trajet et de l'heure de retour prévus. 

\subsubsection{Égarement}

Le conseil habituel -- rebrousser chemin jusqu'à retrouver une marque ou une flèche -- n'est pas des plus attrayants, surtout quand on court à toute vitesse le long d'un sentier mono-trace, mais reste bien souvent valable. Charger la trace du parcours dans sa montre ou dans un autre dispositif limite généralement ce type de soucis. De plus, certaines applications permettent de télécharger des fonds de carte (OpenStreetMap, Institut National de Géographie) et ainsi voir sa position par rapport aux routes et chemins secondaires. Comme décrit précédemment, demander son chemin n'est pas sans risque, encore faut-il rencontrer quelqu'un sur sa route. 

Certains diront que c'est une (petite) île, et que par conséquent il est difficile de perdre. Il suffit d'aller se promener sur les chemins entre La Aldea\index{La Aldea de San Nicolás} et Tejeda\index{Tejeda} pour se rendre compte qu'on peut très bien parcourir 20~km sans voir personne ni même trace de civilisation.

\subsubsection{Chemin impraticable}

Si malgré tout, vous décidez de vous obstiner et ne pas revenir sur vos traces (ce que l'on comprendrait parfaitement), le risque est de vous retrouver soit au bord d'un ravin, et là le demi-tour n'est plus optionnel, soit sur un terrain impraticable de par sa végétation ou se fragilité (terre instable qui s'effrite sous vos pas). 

Le présent livre est dédié à la randonnée et au trail, pas à l'escalade ou la montagne, c'est pourquoi nous recommandons la plus grande prudence aux randonneurs et coureurs qui transitent dans des zones peu peuplées et les chemins non balisés. 

\subsubsection{Panne sèche}

Dernières gouttes d'eau, c'est la catastrophe! Concentrez-vous quelques instants et représentez-vous mentalement cette situation (certainement déjà connue) où vous donneriez presque n'importe quoi pour une verre d'eau fraiche, avec cette gorge pâteuse et ces lèvres sèches. La solution pour éviter la réalisation de cette situation est simple: planifier correctement le déplacement, en oubliant pas de jeter un œil attentif aux prévisions météos. Rien de tels que des températures au-dessus de 35$^{\circ}$ combinées à un vent sec et chaud de l'est pour le rappeler à tous les sportifs en plein air. 

\section{Les compétitions\label{sec:competitions}}
%---------------------------------------------------

Participer à une compétition est une excellente manière de découvrir des endroits souvent peu connus, sans devoir se préoccuper des ravitaillements ou même du parcours. Les clubs de trail organise une fois l'année une compétition sur leurs terrains de jeu favoris, créant ainsi un calendrier de courses des plus variés. Les courses les plus importantes sont organisées par des entités privées, avec souvent une répercussion sur le coût de l'inscription. 

\subsection{Coupes (challenges)}

Tejeda Copa Trail (http://tejedacopatrail.es/) regroupe trois épreuves sur la commune de Tejeda\index{Tejeda}: le "Chronotrail Almendro en Flor", la Circulat de Tejeda et le Plenilunio Tejeda Trail. 

\subsection{Organisation}

Dans la grande majorité des épreuves, l'organisation est soignée jusqu'au moindre détail: balisage parfait, ravitaillements complets et suffisamment fréquents, présence de police pour les traversées de routes, d'ambulances aux points critiques du parcours en cas de problème et de personnel médical et de protection médicale à l'arrivée.

Il n'est pas nécessaire d'être affilié à une fédération pour s'inscrire, par contre certains organisateurs demandent aux coureurs de prendre une assurance spécifique qui les couvre pour le jour de la course (environ 5€). Comme le nombre de coureurs est très souvent limité, il est indispensable de s'inscrire à l'avance, en particulier pour les plus populaires. Même lorsque le quota de coureurs n'est pas atteint, les inscriptions le jour de la course ne sont pas autorisées.

Depuis une dizaine d'années, la fameuse \textit{TransGranCanaria}\index{Transgrancanaria} est organisée vers le mois de mars et s'est convertie en la course montagne la plus populaire de l'île, avec la participation de nombreux professionnels ainsi que d'amateurs provenant de plus de 30 pays. Ce sera l'object de la Section~\ref{sec:tgc}.

Pendant quelques années a aussi eu lieu l'\textit{Ultra Trail de Gran Canaria} (UTGC) dont la modalité la plus longue proposait une distance au delà des 150~km pour 7500~m de dénivelé positif. Toutefois après 2012, l'UTGC n'a plus été organisé. 

\begin{map}
\mysidebox[title={Parcours de la Transgrancanaria entre 2008 et 2018 et de l'Ultra Trail de Gran Canaria entre 2011 et 2012.}]{%
%\includegraphics[width=.6\textwidth]{TGC_UTGC.pdf}
}{%
Les précédentes éditions de la TGC (en vert) ont pris leur départ à Las Palmas de Gran Canaria, Agaete ou Maspalomas, puis gagné le centre de l'île et rejoint à nouveau la côte. Les autres localités fréquemment traversées sont Teror, Tejeda et San Bartolomé de Tirajana. L'UTGC suivait un parcours plutôt orienté est-ouest avec également un passage par le sommet de l'île. 
}
\end{map}

\subsection{Coût}

On utilise parfois le "prix au kilomètre" (prix total divisé par la distance de la course) pour se faire une idée du coût (relatif) d'une course. En règle générale les courses, quelle que soit leur taille, sont plus chères qu'en Belgique, par exemple on ne trouve pas, ou plus, des petites courses locales comme chez nous, avec une vingtaine de kilomètre pour moins de 10 euros.

% ADD "races.tex"



\section{Les traversées\label{sec:traversee}}
%-----------------------

Pour les plus sportifs ou les plus endurants, pourquoi ne pas se frotter à une traversée complète de l'île? La défi est beaucoup plus accessible que la traversée de la Corse par le GR--20 par exemple. Le trajet le plus court entre 2 points situés sur des côtes opposées et d'environ 70~km, permettant ainsi aux coureurs entrainés d'effectuer une traversée en un temps raisonnable. On peut aussi s'inspirer des différents parcours de courses telles que la TransGranCanaria pour avoir des repères. 

\subsection{La TransGranCanaria\label{sec:tgc}}

La Transgrancanaria\index{Transgrancanaria} est l'épreuve la plus connue de l'île. Organisée depuis octobre 2003, elle fait maintenant partie du calendrier mondial du trail, avec la participation de professionels tels que Emilie Forsberg, Ryan Sandes ou Sébastien Chaigneau. Le parcours a évolué au cours des éditions, tout en gardant plusieurs parties communes. Les départs et arrivées ont lieu soit à Las Palmas de Gran Canaria, Maspalomas/Playa el Inglés ou Puerto de las Nieves (Agaete). La plupart des éditions passent par des endroits mythiques comme le Roque Nublo, le Pico de las Nieves, les villages de Teror, Artenara ou de Tunte.

Pour pouvoir y participer, il faut s'y prendre à l'avance car le nombre d'inscriptions est limité. Il faut également également une course de longue distance (détails spécifiés sur leur site web) dans les 2 ans précédant la course. 

\subsubsection{Est-il possible de réaliser le parcours par ses propres moyens?}

La réponse est affirmative, il suffit de bien calculer les quantités de boissons et de nourriture à emmener avec soi, ou bien compter un appui logistique externe. Le parcours traverse de nombreux villages, il est donc possible d'acheter de l'eau et des ravitaillements solides. Il faudra cependant tenir compte des horaires ainsi que de possibles fermetures le dimanche. Dans la mesure du possible, il est préférable de programmer le parcours en semaine. Concernant les ravitaillements, certains parties sont un peu plus critiques:
\begin{enumerate}
\item Entre Agaete et Artenara: environ 35~km les séparent, avec une traversée d'une partie de l'île sans réelle possibilité de trouver un commerce ouvert. Si vous souhaitez sortir léger, une première possibilité serait de se dévier vers El Risco de Agaete. Une autre consiste à déposer la veille eau et aliments dans un endroit sûr, par exemple dans le Parc Naturel de Tamadaba. 
\item Entre Tunte et Maspalomas: le seul endroit où trouver de quoi manger et boire est Ayagaures, où se trouve quelques bars. Il ne fait donc pas hésiter à recharger les réserves d'eau car cette partie du parcours est souvent la plus chaude et la moins ombragée. 
\end{enumerate}

\subsubsection{Quelle route choisir?} 

Il existe plusieurs parcours avec des caractéristiques bien différente. Personnelement je trouve le \textit{Barranco de los Vicentes}, à proximité de Maspalomas particulièrement pénible: bien que relativement plat, le chemin est très technique avec de grosses pièrres sur plusieurs kilomètres. Terminer par cette partie, surtout de nuit, est une expérience peu recommandable. En revanche, commencer depuis Maspaloms permet de faire passer la pilule. L'arrivée sur Las Palmas, via \textit{Los Giles} (voir également Section~\ref{sec:losgiles}, se fait sur un chemin assez roulant, donc même après une centaine de kilomètre dans les jambes, cela devrait aller. 

\subsubsection{Quelles sont les difficultés principales?}

En dehors de la distance, en moyenne 125~km, et du dénivelé positif de l'ordre de 7000~m, la course ne présente pas de difficultés techniques particulières. Le parcours emprunte des chemins connus et correctement signalés, bien qu'il soit indispensable de charger la trace GPS avant de réaliser la route. Autre aspect à considérer: les conditions météorologiques. Lors de l'édition 2017, les variations de températures ont été de l'ordre de 20$^{\circ}$C, avec 6$^{\circ}$C au ravitaillement de Cruce de Ariñez et plus de 25$^{\circ}$ à Tunte et dans le Barranco de los Vicentes. 


\subsection{Les traversées sud-nord}
%-----------------------------------

Si vous souhaitez suivre un itinéraire plus direct pour connecter le nord et le sud de l'île, voici une description des différentes étapes à suivre.

\subsubsection{Étape 1: Maspalomas--Tunte}

La route commence à l'extrême sud de l'île, au phare de Maspalomas. La première étape consiste à atteindre le village de Tunte à 900~m d'altitude. Pour ce faire il existe plusieurs options en fonction du \textit{barranco} emprunté. L'itinéraire proposé est le suivant: Barranco de los Vicentes, Ayagaures, las Tederas, Pilancones, Degollada de la Manzanilla, Tunte. Point de vue difficulté, la montée depuis Pilancones vers la piste qui conduit à la Degollada de la Manzanilla est raide mais courte. Une alternative consiste à suivre la route asphaltée qui conduit à Ayagaures via Montaña de la Data et Monte Leon. Les ravitaillements peuvent s'effectuer à Ayagaures (bars) et Tunte (bars, restaurants et supermarchés).

\subsubsection{Étape 2: Tunte--Pico de las Nieves}

Depuis Tunte on suivra l'itinéraire suivant: Camino de la Cumbre, Cruz Grande, Paso de la Plata, Pargana, Degollada de los Hornos, Degollada de Piedras Blancas et Pico de las Nieves. En option, on peut se dévier du chemin principal pour admirer le panorama depuis la \textit{Ventana del Nublo} et réaliser l'ascencion du Campanario (1917~m), un des plus hauts pics de l'île.

TODO: Ventana Nublo foto

Les ravitaillements lors de cette étape se limitent à la camionette qui vend nourriture et souvenir sur le parking du Pico de las Nieves.

Raccourcis: une fois à la Degollada de los Hornos, on peut poursuivre le sentier jusqu'à los Llanos de la Pez, évitant ainsi l'ascension du Pico de las Nieves.

\subsubsection{Étape 3: Pico de las Nieves--Cruz de Tejeda}

Itinéraire: Llanos de la Pez, el Garañón, Corral de los Juncos, Degolla de la Cumbre, Degollada de Becerra, Degollada de los Molinos, Cruz de Tejeda.
Durant la majorité de cette partie, on profitera d'une vue spectaculaire du Roque Nublo, de la Caldera de Tejeda et si la visibilité le permet, de Tenerife. A noter qu'une fois au niveau de la Degollada de la Cumbre, on entre dans une zone influencée par les alizés, ce qui peut provoquer un changement de température ressentie assez brusque.

Ravitaillements: à la Cruz de Tejeda (bars, restaurants et boutiques touristiques), éventuellement en face du parking de la Degollada de Becerra où se trouve un marchand de souvenirs (uniquement pendant la journée).

\subsubsection{Étape 4: Cruz de Tejeda--Teror}

Itinéraire: Cruz Chica, Barranco de Peñones, Cueva Corcho, Calderetas, Madrelagua, las Rosadas, Teror. 

Ravitaillements: Lanzarote, Valleseco (court détour depuis Madrelagua), Teror.


\subsubsection{Étape 5: Teror--Las Palmas de Gran Canaria}

Itinéraire: Osorio, El Palmar, ...... Los Giles, Playa de las Canteras.




\subsection{Traversée ouest-est}
%-------------------------------

Depuis La Aldea jusqu'à Arinaga.

\subsubsection{Étape 1: La Aldea--Artenara}


\subsubsection{Étape 2: Artenara--Pico de las Nieves}


\subsubsection{Étape 3: Pico de las Nieves--}



\section{Courir dans la capitale}
%--------------------------------

Parfois on n'a pas toujours le loisir de louer une voiture ou prendre le bus, je pense ici à ceux qui se rendent à Gran Canaria pour des motifs professionnels et n'ont pas l'opportunité de l'éloigner de Las Palmas. La sortie classique consiste à courir le long de la plage de Las Canteras. En début de matinée c'est assez calme puis au fur et à mesure de la journée ça peut devenir fort fréquenté aussi bien par les marcheurs que par les coureurs. On peut souvent observer un splendide coucher de soleil avec en arrière-plan l'île de Tenerife et le Teide.

Bien sûr, le parcours de Las Canteras peut s'avérer un peu trop plat et monotone pour les amateurs de trail. On propose donc des alternatives légèrement plus "nature".

\subsection{La Isleta}
%---------------------

\textit{La Isleta} désigne la partie nord de la capitale, au delà de \textit{la Puntilla} (extrémité nord de la plage de Las Canteras\index{Las Canteras}). On peut aisément suivre la côte pour arriver à une autre plage intéressante, \textit{el Confital}, célèbre pour la qualité de ses vagues, comme en témoigne l'abondance de locaux y pratiquant le body-board. C'est aussi un point de vue assez surprenant de Las Palmas ainsi que du reste de l'île. 

Depuis cette plage on peut suivre soit la piste de terre, soit les plateformes en bois installées il y a quelques années, jusqu'à atteindre une clôture qui délimite un domaine militaire, zone à éviter car des entraînements de tir y sont parfois organisés. Pour le retour si on souhaite ajouter un peu de relief, on peut emprunter un des chemins qui montent vers un lotissement, \textit{Las Coloradas}, et terminer au sommet de la petite colline qui surplombe la baie du Confital. De nouveau c'est un point de vue qui mérite l'effort. 

\mysidebox[title={Plage du Confital}]{%
\includegraphics[width=.6\textwidth]{confital.jpg}
}{%
Vue de la plage depuis le sommet de la colline. 
}

\subsection{Los Giles\label{sec:losgiles}}
%---------------------

L'autre option commence à l'autre bout de Las Canteras, près de l'Auditoire Alfredo Kraus. En suivant la côte quelques centaines de mètres, on arrive à proximité d'un grand parking à côté duquel passe une route menant vers l'intérieur du barranco. Après environ 2~km, sur la droite se trouve une piste menant vers le lotissement de Los Giles, à un peu moins de 300~m d'altitude. Tout autour de la zone habitée, vous trouverez une multitude de pistes et chemins dont les combinaisons vous permettent de nombreux parcours et distances. 

\mysidebox[title={Vue depuis Los Giles}]{%
\includegraphics[width=.6\textwidth]{losGilesVista.JPG}
}{%
La baie de Las Canteras avec en arrière-plan La Isleta. 
}

Celui que l'on propose se déroule autour de Los Giles et suit essentiellement des chemins de terre. La zone est fréquentée par les parapentistes, que vous verrez certainement survoler la plage de Las Canteras. N'oubliez pas de profiter de point de vue sur la ville et sur les trois volcans de La Isleta.

\begin{anecdote}
Sur ce chemin vous pourrez aussi observer la piste d'un petit aérodrome. Celui-ci est tristement célèbre pour un accident qui s'est produit en ... TODO add description
\end{anecdote}


\begin{tcolorbox}[left=1mm,right=1mm,top=1mm,colback=white,colframe=Green,colbacklower=yellow!10,title="Valsequillo"]
%\includegraphics[width=\textwidth]{losGiles.png}
\tcblower
%\includegraphics[width=\textwidth]{./figures/profil_NocheMagica.png}
\end{tcolorbox}


\subsection{Barranco de Guiniguada}
%----------------------------------

Si vous vous trouvez dans la zone de Triana-Vegueta, le Barranco de Guiniguada est une option à considérer. Imaginez qu'il y quelques décénies, l'eau y coulait comme dans un fleuve. Au début le chemin est une piste de terre passant à proximité de plantations de bananes, qui se fait ensuite plus étroite pour se convertir en chemin couvert de pierres. Notons que c'est un des chemins suivis pour atteindre la \textit{cumbre} (centre de l'île) depuis la capitale, en passant par Santa Brigida puis San Mateo, bien qu'il ne soit pas souvent utilisés lors des trails. La raison est double: premièrement, jusqu'il y a peu le chemin était marqué mais très mal entretenu: on devait passer au milieu d'une végétation abondante et sur une sol couvert de pavés ou de grosses pierres. L'autre raison est liée à la forme du barranco: pendant la journée la température peut y monter très rapidement, sans offrir de zones à l'ombre.

\begin{anecdote}
Lors d'un ultratrail qui terminait par la descente du Guiniguada, je me trouvais au ravitaillement de Santa Brigida lorsqu'un autre coureur me dit qu'il fallait impérativement que je remplisse le réservoir de mon sac à dos de glaçons afin de pouvoir traverser cette fournaise.
\end{anecdote} 

\section{Des chemins à éviter?}

C'est une question que je me pose souvent: est-ce qu'il y aurait une course ou un chemin que je déconseillerais à des amis? 

La réponse n'est pas directe, et ce pour plusieurs modifs:
\begin{enumerate}
\item Tout le monde a ses préférences pour les parcours. Concrètement, je n'ai vraiment pas apprécié le chemin entre Maspaloma et Ayagaures, en particulier le \textit{Barranco de los Vicentes}, car celui-ci est couvert de grosses pierres sur lesquelles il est difficile (ou risqué) de courir, et le paysage est relativement monotone. Le fait que les derniers kilomètres de la TransGranCanaria passent par là a peut-être joué un rôle aussi.
De même, certains parcours vertigineux peuvent plaire à certains et faire peur à d'autres.
\item Les chemins évoluent au cours du temps: parfois ils sont totalement couvert de végétation, d'autres fois ils ont été restaurés par les autorités communale, parfois le marquage laisse à désirer\ldots Tout cela pour dire qu'un chemin en mauvais état un jour peut devenir très agréable la semaine suivante.
\item Le ressenti sur un parcours ne dépend pas que du parcours lui-même mais aussi de facteurs externes, en particulier l'état de fatigue du coureur, ou bien les conditions météorologiques. C'est pour cela que généralement les routes dans le sud de l'île sont moins conseillées en été, et que les chemins de la \textit{cumbre} ne sont pas toujours agréables les jours les plus frais et pluvieux de l'hiver.
\end{enumerate}

Au niveau des compétitions, il n'y a vraiment aucune que je déconseillerais, tant les organisations sont irréprochables et les parcours parfaitement dessinés pour faire découvrir une partie de l'île, même celles qui sont organisées pour la première fois. 


\section*{Sources d'information}


\section{Sources de données}

Institut Géographique National espagnol (IGN): \url{http://www.ign.es/csw-inspire/srv/eng/main.home}
 
Agence Nationale de Météorologie (AEMET): \url{http://www.aemet.es/es/portada}

Sondages atmosphériques: University of Wyoming: \url{http://weather.uwyo.edu/cgi-bin/sounding?region=africa&TYPE=TEXT%3ALIST&YEAR=2018&MONTH=07&FROM=2600&TO=2600&STNM=60018}

\url{http://www.gobiernodecanarias.org/politicaterritorial/temas/espaciosnaturales/informacion/grancanaria/}

\url{https://www.gobiernodecanarias.org/planificacionterritorial/temas/informacion-territorial/enp/categorias/parquesrurales/}

\url{https://www3.gobiernodecanarias.org/medusa/wiki/index.php?title=Red_Canaria_de_Espacios_Naturales_Protegidos#Paisajes_Protegidos}

Données satellitaires d'incendie: \url{https://firms.modaps.eosdis.nasa.gov/download/}

Académie Canarienne de la langue: \url{http://www.academiacanarialengua.org}

Toponymes de Gran Canaria (blog): \url{http://toponimograncanaria.blogspot.be/2012/03/toponimos-genericos-de-la-isla-se.html}

L'histoire de Saint Jacques à Gáldar: \url{https://jacobeogaldar.es/jacobeo2021/historiajacobea}

Les espaces naturels: \url{https://www.holaislascanarias.com/espacios-naturales/gran-canaria/}

\printindex

\end{document}

