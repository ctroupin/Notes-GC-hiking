

\chapter{Organiser vos randonnées à Gran Canaria}
\AddToShipoutPicture*{\BackgroundIm{Photos/chapter2.jpg}}
\thispagestyle{empty}
\newpage
%------------------------------------------


\section{Signalisation et balisage\label{sec:signalisation}}
%-----------------------------------------------------------

S'il fallait résumer la signalisation des chemins en quelques mots, ce serait "une combinaison du meilleur et du pire". En effet, on peut trouver de nombreux parcours parfaitement marqués et agrémentés de panneaux explicatifs des points d'intérêt, tout comme on peut se retrouver avec pour seul guide, les tas de pierre dressés par les précédents randonneurs. Le cas le plus litigieux et celui, assez fréquent, où les seules indications se trouvent en début de randonnée, au milieu d'un village, ce qui laisse penser que la suite sera aisée, pour ensuite ne trouver aucune indication lors des différents croisements. 

\begin{infobox}{Exemples de flèchage.}
\includegraphics[width=.33\textwidth]{flechas/flechasCabildo4.JPG}~\includegraphics[width=.33\textwidth]{flechas/flechasCabildo2.JPG}~\includegraphics[width=.33\textwidth]{flechas/flechasArtenara.jpg}

\includegraphics[width=.33\textwidth]{flechas/flechasTejeda}~\includegraphics[width=.33\textwidth]{flechas/flechasCaseras2.JPG}~\includegraphics[width=.33\textwidth]{flechas/flechasCaseras.JPG}

\includegraphics[width=.33\textwidth]{flechas/flechasCulata.JPG}~\includegraphics[width=.33\textwidth]{flechas/flechasTaidia.JPG}~\includegraphics[width=.33\textwidth]{flechas/flechasSantiago}

\tcblower

À côté du fléchage officiel (première case), on trouve une multitude de balisages, des plus rudimentaires (deuxième rangée) aux plus sophistiquées et durables (troisième rangée). 
\end{infobox}	

\subsection{Marquage "Cabildo"}
%-------------------------------------

Le marquage officiel est illustré ci-dessous et consiste en un poteau en bois munis d'une ou plusieurs flèches du même matériau dans lesquelles sont gravés les différentes directions que suivent les chemins. Les codes de couleurs sur le côté de la flèche indiquent le type de route: en vert, les chemins locaux (SL étant l’abréviation de \textit{Sendero Local}, c'est-à-dire sentier local), tandis que le jaune indique les chemins de moyenne distance, avec une numérotation globale sur l'île. A noter que toutes les flèches ne sont pas munies de ce code couleur.
\mysidebox[title=Signalisation de type "Cabildo".]{%
\includegraphics[width=.45\textwidth]{GranCanaria2017_1539.JPG}
}{%
Marquage officiel du Cabildo sur le parcours qui relie Teror à la Cruz de Tejeda. La couleur vert (SL) indique un chemin local, la jaune (S-14) un chemin de moyenne durée.
}
Ce type de marquage présente le désavantage d'être facilement détruit ou enlevé, il ne faut dès lors pas s'étonner de se retrouver face au poteau central sans aucun fléchage.

\subsection{Lignes de couleur}
%-----------------------------

Dans certains cas on peut encore trouver des marques de peintures sur des rochers, des murs voire des arbres, similaire à ceux définis dans les normes internationales. Toutefois ces marquent relèvent plutôt de l'anecdote et ont tendance à disparaître avec le temps, étant donnée l'absence d'entretien de ce type de signalisation.

\mysidebox[title=Signalisation avec lignes de couleurs.]{%
\includegraphics[width=.45\textwidth]{marquage_peinture.JPG}
}{%
Marques de couleur au niveau de la \textit{Degollada de la Cumbre}. On trouve des marques similaires dans la descente qui mène vers le patelain de \textit{La Culata} (Tejeda).
}

\subsection{Marquage municipal ou local}
%---------------------------------------

Par marque municipal, on entend une signalisation dont les caractéristiques sont spécifiques à une municipalité donnée ou à un group de municipalités d'une même zone géographique. Bien sûr, ce type de marquage concerne surtout des parcours relativement courts. 

\mysidebox[title=Signalisation de type "municipal".]{%
\includegraphics[width=.5\textwidth]{flechas_artenara}
}{%
Flèchage à Artenara. La fiche technique inclut une description de la route en Espagnol, Anglais et Allemand ainsi que les caractéristiques générales (longueur, durée, niveau de difficulté).
}

Un bon exemple est le réseau de chemins créés et balisés par l'\textit{Ayuntamiento de Artenara}, dans le nord-ouest de l'île. Ce sont ici des poteaux en bois munis de plaques métalliques qui indiquent la direction ainsi que les distances jusqu'aux différents points de référence. Des panneaux descriptifs montrant cartes, profils et photos sont également installés.

\subsection{Les monticules de pierres}
%-------------------------------------

Les groupes de marcheurs ont l'habitude de créer de petits tas de pierres, appelé ici \textit{mojones}, le long des chemins peu ou mal balisés. Le revers de la médaille est que ces tas ont tendance à se multiplier dans des endroits où ils n'ont aucune utilité.

\mysidebox[title=Tas de pierre ou \textit{mojón}]{%
\includegraphics[width=.45\textwidth]{tasdePierre}
}{%
Indication de la déviation vers le \textit{Barranco del Negro}.
}


\section{Sources d'information}
%------------------------------

Comme décrit dans l'introduction, ce livre n'a pas pour but de décrire en détail une multitude de chemins, mais plutôt de fournir les outils nécessaires à la préparation de sorties randonnées. 

Il existe aujourd'hui plusieurs sites internet et blogs dédiés à la randonnée à Gran Canaria, la plupart d'entre eux en espagnol et quelques uns en anglais. Parmi les 21 municipalités, nombreuses sont celles qui proposent, via leur département de tourisme, une liste de chemins de randonnées. 

Enfin, les portails de partage d'itinéraires, tels que wikiloc (\url{www.wikiloc.com}) ou movescount (\url{www.movescount.com}), ont l'avantage d'afficher les informations sur un carte et permettent le téléchargement des traces GPS vers un dispositif mobile (montre, téléphone, tablette). Tous les parcours proposés dans ce livre sont d'ailleurs mis à disponibilité du lecteur via la plateforme wikiloc à l'adresse suivante: \url{www.wikiloc.com}.


\subsection{État général des chemins\label{sec:etatschemins}}
%---------------------------------------------------------

Comme on pouvait s'y attendre en analysant les marquages, l'état et la qualité des chemins dépendent fortement de leur localisation, mais peuvent aussi varier au cours du temps. On dit d'ailleurs que la manière de maintenir un chemin dans de bonnes conditions, c'est de faire en sorte qu'il soit fréquenté par les randonneurs. Vous pourrez mieux vous en rendre compte si vous suivez un chemin qui a été parcouru récemment par une course.

Certains chemins demandent une attention particulière car ils peuvent se voir affectés lors d'épisodes de fortes précipitation. C'est par exemple le cas du \textit{Camino de la Plata} (voir Section~\ref{sec:caminoplata}) dont le pavage avait été dégradé par les pluies de l'hiver 2016-2017. Il avait été rapidement remis en état afin de pouvoir permettre le passage en toute sécurité participants de la course \textit{TransGranCanaria}, qui avait lieu quelques semaines plus tard.

\subsection{Où et comment s'informer?}
%-------------------------------------

Il n'existe pas de numéro de téléphone ou d'adresse email où l'on peut s'informer des conditions de praticabilité des sentiers. Néanmoins, le Cabildo de Gran Canaria\index{Cabildo}, à travers des départements d'Environnement (\textit{Medio Ambiente}) et de Routes (\textit{Carreteras}) publie généralement des informations via les réseaux sociaux ainsi que dans les journaux.

Si des éboulement se sont produits sur une route à proximité de la randonné prévue, il est probable que les chemins aient subi quelques dommages.

\subsection{Comment se déplacer sur l'île?}
%--------------------------------

\paragraph{Bus:} Deux compagnies principales de bus publiques (appelés ici \textit{guaguas}) offrent leurs services sur l'île: 
\begin{enumerate}
\item \textit{Guagua Municipal} (\url{https://www.guaguas.com/}) dont les lignes desservent essentiellement la capitale et sa périphérie.
\item \textit{Global} (\url{https://www.guaguasglobal.com/}) qui relie les différents municipalités.
\end{enumerate}
À noter que leurs sites internets sont uniquement disponibles en espagnol et en anglais.

La ligne la plus intéressante du point de vue de la randonnée est certainement la ligne 18 qui relie San Mateo à Maspalomas, en passant par la Cruz de Tejeda, Tejeda, San Bartomolé de Tirajana (Tunte) et Fataga, avec toutefois des horaires assez limités. Il est dès lors conseillé de bien planifié son excrusion si vous prévoyez d'utiliser ce mode de transport. 

Depuis la capitale, on peut également utiliser la ligne 305 qui conduit à San Mateo environ toutes les 30 minutes.

\paragraph{Taxis:} Les taxis sont une excellente option pour les trajets à l'intérieur de Las Palmas de Gran Canaria, ou depuis l'aéroport. Par contre ils sont rarement utilisés par les randonneurs dans le centre de l'île, étant donnés leur disponibilité limitée ainsi que leurs tarifs. 


\paragraph{Voitures de location:} La location de véhicules est certainement la solution qui fournit le plus de flexibilité. Même les habitants de l'île y ont régulièrement recours. Il existe des dizaines d'entreprises fournissant ce service. Il faudra être attentif non seulement aux tarifs, mais aussi aux conditions mentionnées dans les assurances.


\subsection{Conduire à Gran Canaria}

Le code de la route en Espagne ne présente pas de différences significatives par rapport à la Belgique ou la France. Si vous circulez sur autoroute, notez qu'il est très fréquent que les voitures occupent préférentiellement la bande centrale, même si celle de droite n'est pas occupée. Lorsque vous emprunter les routes plus touristiques de l'intérieur de l'île, vous pourrez régulièrement rencontrer des conducteurs qui circulent au milieu de la chaussée, peut-être par manque d'habitude de la conduite sur des routes de montagne. 

On ajoutera que les pompes à essence sont rarement automatiques en dehors de la capitale, ce qui veut dire qu'il est judicieux de prévoir un arrêt à la pompe avant la tombée de la nuit. Certains compagnies de locations fournissent des cartes de l'île avec les localisations des pompes.

La table~\ref{tab:distances} fournit une vue d'ensemble des distances entre les municipalités. Les plus proches sont Ingenio et Aguimes (sud-est) avec 3 kilomètres, tandis que la Aldea et Galdar sont distantes de 112~km. Il faut évidemment nuancer ces informations sur les distances, par exemple les 48 kilomètres qui séparent Agaete à la Aldea vous paraîtront certainement beaucoup plus longs que les 86 kilomètres entre Las Palmas de Gran Canaria et Santa Lucía.

L'autoroute principale de l'île relie le centre touristique de Maspalomas-Playa el Inglés à la capitale en passant par l'aéroport et Telde. L'autoroute du nord permet de rejoindre Agaete via Arucas et Gáldar. La partie occidentale de l'île, entre Agaete et Mogán, est caractérisée par une route spectaculaire et sinueuse, parfois appelée la \textit{Route au 365 Virages}, contournant les obstacles topographiques de la côte ouest. Lors des épisodes de pluie importants, la route entre Agaete et la Aldea se trouve fréquemment coupée suite à des éboulements sur la chaussée. Après des années de revendications, les habitants de la Aldea ont enfin obtenu que des travaux de modernisation du réseau routier aient lieu en direction d'Agaete avec le création de nouvelles voies et de tunnels. 

\begin{landscape}
\begin{table}
\footnotesize
\caption{Distance en kilomètres entre 2 municipalités.\label{tab:distances}}
\rowcolors{2}{gray!25}{white}
\begin{tabular}{lccccccccccccccccccccc}
& \rot{Agaete}
& \rot{Aguimes}
& \rot{Artenara}
& \rot{Arucas}
& \rot{Firgas}
& \rot{Gáldar}
& \rot{Ingenio}
& \rot{La Aldea de San Nicolás}
& \rot{Las Palmas de GC}
& \rot{Mogán}
& \rot{Moya}
& \rot{San Bartolomé}
& \rot{Santa Brígida}
& \rot{Santa Lucía}
& \rot{Santa María de Guía de GC}
& \rot{Tejeda}
& \rot{Telde}
& \rot{Teror}
& \rot{Valleseco}
& \rot{Valsequillo}
& \rot{Vega de San Mateo}\\
\midrule
Agaete &\ef& 72& 34& 28& 32& 10& 63& 48& 35& 60& 27& 70& 48& 87& 11& 45& 52& 38& 42& 58& 55\\
Agüimes & 72&\ef& 50& 50& 57& 64& 3& 81& 39& 61& 63& 26& 40& 19& 63& 35& 21& 52& 40& 27& 47\\
Artenara & 34& 50&\ef& 44& 39& 42& 50& 40 & 58& 51& 33& 42& 40& 51& 43& 18& 53& 36& 30& 49& 34\\
Arucas& 28& 50& 44&\ef& 8& 20& 41& 75& 15& 97& 15& 55& 27& 65& 19& 31& 30& 11& 15& 36& 32\\
Firgas& 32& 57& 39& 8&\ef& 24& 48& 69& 22& 65& 11& 50& 34& 59& 18& 26& 37& 11& 9& 43& 25\\
Gáldar& 10& 64& 42& 20& 24&\ef& 56& 56& 28& 112& 20& 75& 41& 79& 14& 43& 45& 30& 34& 50& 48\\
Ingenio& 63& 3& 50& 41& 48& 56&\ef& 85& 30& 65& 54& 29& 31& 22& 55& 35& 18& 43& 40& 18& 35\\
La Aldea de San Nicolás& 48& 81& 40 & 75& 69& 56& 85&\ef& 107& 20& 74& 52& 108& 47& 54& 34& 63& 53& 47& 56& 46\\
Las Palmas de GC& 35& 39& 58& 15& 22& 28& 30& 107&\ef& 86& 26& 61& 16& 54& 27& 34& 19& 19& 29& 25& 22\\
Mogán& 60& 61& 51& 97& 65& 112& 65& 20& 86&\ef& 90& 37& 87& 46& 70& 38& 74& 61& 55& 80& 44\\
Moya& 27& 63& 33& 15& 11& 20& 54& 74& 26& 90&\ef& 53& 40& 69& 7& 29& 43& 18& 19& 49& 34\\
San Bartolomé de Tirajana& 70& 26& 42& 55& 50& 75& 29& 52& 61& 37& 53&\ef& 38& 8& 56& 24& 49& 47& 41& 41& 30\\
Santa Brígida& 48& 40& 40& 27& 34& 41& 31& 108& 16& 87& 40& 38&\ef& 55& 40& 23& 20& 20& 17& 22& 7\\
Santa Lucía de Tirajana& 87& 19& 51& 65& 59& 79& 22& 47& 54& 46& 69& 8& 55&\ef& 77& 33& 41& 55& 50& 40& 39\\
Santa María de Guía de GC& 11& 63& 43& 19& 18& 14& 55& 54& 27& 70& 7& 56& 40& 77&\ef& 38& 43& 28& 32& 49& 46\\
Tejeda& 45& 35& 18& 31& 26& 43& 35& 34& 34& 38& 29& 24& 23& 33& 38&\ef& 39& 23& 16& 33& 19\\
Telde& 52& 21& 53& 30& 37& 45& 18& 63& 19& 74& 43& 49& 20& 41& 43& 39&\ef& 32& 44& 7& 21\\
Teror& 38& 52& 36& 11& 11& 30& 43& 53& 19& 61& 18& 47& 20& 55& 28& 23& 32&\ef& 6& 34& 17\\
Valleseco& 42& 40& 30& 15& 9& 34& 40& 47& 29& 55& 19& 41& 17& 50& 32& 16& 44& 6&\ef& 31& 15\\
Valsequillo& 58& 27& 49& 36& 43& 50& 18& 56& 25& 80& 49& 41& 22& 40& 49& 33& 7& 34& 31&\ef& 16\\
Vega de San Mateo& 55& 47& 34& 32& 25& 48& 35& 46& 22& 44& 34& 30& 7& 39& 46& 19& 21& 17& 15& 16&\ef\\
\bottomrule
\end{tabular}
\end{table}
\end{landscape}




\section{Quelques conseils avant de prendre la route}
%------------------------------------------

Les recommandations générales, bien connues des randonneurs, constituent un bon point de départ:
\begin{itemize}
\item Emporter eau et nourriture en suffisance, en accord avec les conditions météorologiques (Section~\ref{sec:meteo} page~ref{sec:meteo}) et la difficulté de la route.
\item Adapter les chaussures et vêtements en fonction du parcours.
\item Ne pas sortir seul, et dans la mesure du possible, informer quelqu'un du parcours qui sera effectué ainsi que de l'heure approximative de retour.
\item Éviter de sortir des chemins marqués.
\item Ne laisser aucun déchet derrière vous et ne pas cueillir de fleurs.
\end{itemize}
Les spécificités de Gran Canaria impliquent toutefois de s’attarder un peu plus sur certains points.

\subsection{Ravitaillements}
%------------------------------

Emporter une quantité d'eau suffisante pour les longs trajets peut être assimilé à transporter un sac à dos de poids excessif, raison pour laquelle il est souvent préférable de prévoir différents points de ravitaillement. 

Dans le partie nord de l'île se trouvent de nombreux villages dans lesquels on trouve aisément un bar ou une petite épicerie. Au fur et à mesure que l'on s'approche du centre et la \textit{cumbre}, la distance entre village augmente et sur la  partie la plus haute de l'île, les seules possibilité d’approvisionnement sont les vendeurs "touristiques" et temporaires, situés par exemple aux parkings du Pico de las Nieves, du Roque Nublo ou du Mirador de Becerra. Il faudra évidemment tenir compte de l'époque de l'année, de l'heure de la journée et des conditions météorologiques pour pouvoir  profiter de cette solution.

À noter que les noyaux de population sont souvent situés à proximité des côtes, ce qui signifie dans le cas de San Bartolomé de Tirajana par exemple, que la partie nord de la municipalité est beaucoup moins peuplée. 

Effectuer des randonnées dans ces parties de l'île requiert donc un minimum de logistique.

\subsection{Sources d'eau}
%-------------------------

Les sources d'eau potable sont plutôt rares et il ne vaut donc pas mieux trop compter sur elles. Le qualificatif "potable" est d'ailleurs à prendre avec prudence: si le caractère "potable" n'est pas indiqué explicitement, c'est au risque du consommateur. Une des sources se trouve dans le \textit{Barranco de la Mina}, à proximité d'Utiaca, près de San Mateo. Elle est située dans un virage à proximité de la route principale, et il n'est pas rare d'y voir des locaux remplir des bidons. La carte~\ref{map:water} reprend les positions de différentes sources. Il est important de noter qu'il n'est pas garanti que chacune de ces sources fonctionnera: il est possible, voire probable, que certaines soient à sec ou que l'eau ne soit plus potable. C'est pourquoi il faut insister sur la préparation de la route et et de la quantité d'eau nécessaire en fonction de cette dernière. 

\begin{map}
\includegraphics[width=\textwidth]{gc_sources01.png}
\caption{Localisation des sources d'eau potable.\label{map:water}}
\end{map}


\subsection{Chasse}

Une autre activité à prendre en compte lors d'excursion est la chasse: la période de chasse est modifiée chaque année et dépend également de la modalité et du type de proie (lapins, perdrix, colombes). En général elle s'étale depuis le début du mois d'août jusqu'à la fin octobre, les battues ont lieu les jeudis et dimanches uniquement. Cette activité est régie par la Fédération Canarienne de Chasse (Federación Canaria de Caza, \url{}), qui compte une délégation dans chaque île. Le nombre de chasseurs fédérés est proche des 20.000, répartis sur une septantaine d'associations ou de clus.

Les chasseurs sont accompagnés de chiens (\textit{podenco canario}) et équipés des fusils (\textit{escopetas}). Il existe différentes zones, certains réservées exclusivement à la chasse, d'autres où elle est totalement interdite de manière à y protéger la faune. Vous rencontrerez ou entendrez probablement les chiens avant de croiser leurs maîtres et il peut alors être judicieux de rebrousser chemin. Dans la mesure du possible, il convient d'éviter les zones et périodes de chasse même si jusqu'à ce jour aucun incident n'est à déplorer.


\subsection{En cas d'égarement}
%------------------------------

Si vous disposez du temps et de l'énergie nécessaires, se perdre constitue une excellent façon de découvrir de petites merveilles, pour autant qu'après vous puissiez retrouver votre chemin. Une erreur fréquente consiste à croire que tous les habitants de l'île sont des randonneurs et qu'ils connaissent parfaitement les alentours de leur maison. Si vous demandez votre chemin à quelqu'un, on préférera vous donner des informations incomplètes ou variablement éloignées de la réalité qu'avouer qu'on ne sait pas. N'interprétez évidemment pas cela comme une intention négative et appliquez plutôt la règle suivante: multiplier le temps indiquer pour atteindre une endroit par 3 voire 4 pour obtenir une estimation plus proche de la réalité.

\begin{anecdote}
Un jour où je devais rentrer à Valleseco depuis Ariñez, je contacte des amis qui m'indiquent que le trajet ne prendra qu'une demi-heure. En réalité, il m'aura fallu plus d'une heure et demie. Un autre jour depuis Ayacata, une vieille dame d'une petite boutique me dit que Valleseco n'est qu'à une demi-heure de marche. Même en voiture il n'est pas facile de le faire dans ce temps!
\end{anecdote}


\subsection{En cas de problèmes\ldots}

Si un accident se produit, le numéro à composer est le 112, qui vous redirigera vers le service compétent en fonction de la gravité de la situation. Dans les zones sans couverture, si vous disposer d'une radio-émétrice, utilisez le CANAL 7 SUBTONO 7 (446.08125 Mhz Subtono 85.4), uniquement en cas d'urgence (\url{http://www.canal77pmr.com} pour plus de détails). Le signal morse de S.O.S. se compose des 3 sifflements courts, trois longs et de nouveau trois courts: S = $\cdot~\cdot~\cdot$, O = $\textendash~\textendash~\textendash$ , S = $\cdot~\cdot~\cdot$.

Si vous observez des dommages à des structure arquéologiques, il est recommandé de contacter la \textit{Guardia Civil} (gendarmerie) au numéro 062 et d'en informer la Département du Patrimoine du Cabildo de Gran Canaria (\url{patrimonio@grancanaria.com}). 


\chapter{Guide de randonnées}
\AddToShipoutPicture*{\BackgroundIm{Photos/chapter3.JPG}}
\thispagestyle{empty}
\newpage

%------------------------------------------

Nous entrons dans le vif du sujet dans cette partie du partie du guide en proposant une une série de randonnées, classées par thèmes en fonction de leur caractéristiques. La liste est loin d'être exhaustive, elle a plutôt pour objectif de donner des idées et de montrer différent types de parcours en fonction des profile de tout un chacun. À titre d'illustration, l'\textit{Ayuntamiento de San Bartolomé de Tirajana} propose sur sa page web près d'une centaine de randonnées! Si chaque municipalité faisait de même, ce seraient plus de 2000 chemins de randonnées mis à disposition des touristes et des locaux.


\section{La Cruz de Tejeda\label{sec:cruztejeda}}

Depuis ce lieu partent des randonnées dans toutes les directions, raison pour laquelle la \textit{Cruz de Tejeda} est considérée comme le point central du réseau des chemins de randonnées. C'est presqu'un passage obligé pour les randonneurs. Depuis ce point on peut aisément se rendre à Artenara, Moya, Valleseco, Teror, San Mateo, Tunte, ou Tejeda. Pour preuve, quasiment toutes les éditions de la fameuse course \textit{Transgrancanaria} (Section~\ref{sec:competitions}, page~\pageref{sec:competitions}) y sont passées.

On y trouve un point d'information touristique, quelques bars et restaurants, de petites échoppes vendant fruits, boissons et autres souvenirs (parfois sans aucun rapport avec les Canaries), et le Parador National de tourisme, un hôtel avec restaurant, piscine et spa, récemment restauré suite aux dégâts infligés par l'incendie de septembre 2017 (page~\pageref{sec:incendie}).

C'est aussi un endroit prisé par les photographes, spécialement au couché de soleil, étant données les superbes vues vers la Caldera de Tejeda et Tenerife à l'ouest, et vers las Lagunetas et le Barranco de la Mina à l'est. Enfin, de par son altitude (1510~m) et sa position face aux vent alizées, la Cruz de Tejeda est régulièrement un endroit de transition d'un air frais et humide à un air plus chaud et sec du côté de la Caldera de Tejeda. Il n'est pas rare d'y observer des cascades de nuages et des épisodes de pluie horizontale (page~\pageref{sec:pluiehorizontale}).

\begin{infobox}{Miguel de Unamuno.}
\includegraphics[width=.49\textwidth]{unamuno1.JPG}
\includegraphics[width=.49\textwidth]{unamuno2}
\tcblower
L'écrivain et philosophe (1864--1936) a laissé une trace importante lors de son passage à Gran Canaria en 1910, comme en témoignent les divers monuments en son honneur. Après son arrivée à Las Palmas, il s'est rendu succéssivement à Teror, Valleseco, Cruz de Tejeda, Artenara et les Tilos de Moya. Le Mirador de M. de Unamuno a été inauguré à Valleseco il y a quelques années. Le mirador est orné d'une sculpture dans la pierre, œuvre du tailleur \'{A}ngel Rosario Hernández, dont l'atelier se trouve dans le quartier \textit{El Zumacal}, à quelques kilomètres de là. La version originale montrait Unamuno portant des lunettes, mais une version sans lunette a été préférée.

Un autre monument dédié à l'écrivain se trouve à Artenara. Il s'agit ici d'une statue de bronze montrant Unamuno observant la Caldera de Tejeda et la décrivant comme une "tempête pétrifiée" (\textit{tempestad pretrificada}), formulation que l'on retrouve aussi près de la Cruz de Tejeda. Dans les rues de Teror se trouve également une plaque rappelant le séjour de l'écrivain. 
\end{infobox}


\section{Les classiques}
%------------------------

Ces parcours sont décrits dans de nombreux ouvrages de randonnées et ne présentent pas de difficultés particulière, ni au niveau de la signalisation, ni au niveau du terrain. Il convient toutefois de respecter les habituelles consignes de sécurité, rappelées à la fin du chapitre précédent.

Nous les avons choisis car ils offrent une bonne première approche des chemins dans la partie centrale de Gran Canaria.

\subsection{Llanos de la Pez - Roque Nublo}
%------------------------------------------

L'aire récréative des \textit{Llanos de la Pez} est l'endroit parfait pour un pic-nic ou un barbecue, l'endroit est équipé de table et de bancs, de toilettes et de grilles pour le barbecue.



\subsection{Camino de la Plata\label{sec:caminoplata}}
%------------------------------

\subsection{Cruz de Tejeda - Valleseco - Teror}
%--------------------------------------

Le chemin commence à côté du parking de la Cruz de Tejeda\index{Cruz de Tejeda}, avec une montée qui nous emmenera vers la Degolla de Cruz Chica\index{Cruz Chica} (1610 m), point du culminant du parcours. Une fois la route traversée, on continuera la descente au départ sur un chemin taillé dans la pierre, ensuite sur un sentier plus régulier.  


\begin{infobox}{Les croix.}
\includegraphics[width=.25\textwidth]{cruz/cruzTejeda}~\includegraphics[width=.25\textwidth]{cruz/cruzNavegante.JPG}~\includegraphics[width=.25\textwidth]{cruz/cruzSanMateo.jpg}~\includegraphics[width=.25\textwidth]{cruz/cruzMoriscos}

\includegraphics[width=.25\textwidth]{cruz/cruzArinez}~\includegraphics[width=.25\textwidth]{cruz/cruzSocorro}~\includegraphics[width=.25\textwidth]{cruz/cruzSiglo}~\includegraphics[width=.25\textwidth]{cruz/cruzMontanon}

\includegraphics[width=.25\textwidth]{cruz/cruzCarpio}~\includegraphics[width=.25\textwidth]{cruz/cruzChica}~\includegraphics[width=.25\textwidth]{cruz/cruzSantiago.JPG}~\includegraphics[width=.25\textwidth]{cruz/cruzToscon.JPG}

\includegraphics[width=.25\textwidth]{cruz/cruzGaldar}~\includegraphics[width=.25\textwidth]{cruz/cruzGrande.JPG}~\includegraphics[width=.25\textwidth]{cruz/cruzHoyaAlta.JPG}~\includegraphics[width=.25\textwidth]{cruz/cruzPargana.JPG}

\includegraphics[width=.25\textwidth]{cruz/cruzSolana.JPG}~\includegraphics[width=.25\textwidth]{cruz/cruzTimagada.JPG}~\includegraphics[width=.25\textwidth]{cruz/cruzMogan.JPG}~\includegraphics[width=.25\textwidth]{cruz/cruzChorillo.JPG}
\tcblower
Les croix font partie intégrante du paysage rural canarien. Les plus connues et visibles sont celles de Tejeda (voir page~\pageref{sec:cruztejeda}) et de Santiago, dans le village de Tunte. Les plus hautes sont situées sur le Roque Saucillo à plus de 1800~m d'altitude. Les plus simples sont faites de bois, d'autres sont métalliques et quelques unes taillées dans la pierre.
\end{infobox}

En 1944, le gouvernement du dictateur Franco a demandé que soit effectué un ressencement des croix qui subsistaient afin de pouvoir ensuite les restaurer. Les bourgmestres de certains communes ont répondu à cette requête en envoyant une liste complète des croix situées sur leur territoire, tandis que d'autres ont simplement répondu qu'il n'y en avait aucune.

\begin{anecdote}
\textit{Valleseco} (la Vallée Sèche en français) située dans le nord de l'île à environ 1000~m d'altitude est souvent mentionné comme l'endroit où il pleut le plus à Gran Canaria. L'origine du nom vient du fait que la vallée centrale du village se trouve entre deux \textit{barrancos} plus longs et plus profonds: le \textit{Barranco de la Virgen}\index{Barranco de la Virgen} à l'ouest et le \textit{Barranco de Madrelagua} à l'est. La qualificatif "seco" est donc tout relatif. Auparavant le village était appelé \textit{San Vicente de Abajo}, et c'est à partir du XVIIIème siècle que le toponyme Valleseco est apparu.
\end{anecdote}
 


\section{Les circulaires}
%------------------------

Quelques parcours qui ne vous obligent pas à organiser un retour vers votre véhicule.

\subsection{Presa de Chira}
%--------------------------


\subsection{Circulaire Llanos de la Pez - Pico de las Nieves}
%------------------------------------------------------------

Cette route circulaire permet d'atteindre la partie la plus haute de l'île, à proximité du Pico de las Nieves\index{Pico de las Nieves} et fait partie des chemins balisés par le Cabildo. La plupart du temps elle parcourt une zone de pins canariens. On peut observer le Roque Nublo et l'île de Tenerife\index{Tenerife} en arrière-plan.


\subsection{Pinar de Tamadaba}
%-----------------------------

https://www.holaislascanarias.com/senderos/gran-canaria/bajada-de-faneque-tamadaba/


\section{Les vertigineuses}
%--------------------------

Nous nous adressons ici aux amateurs de sensations fortes, autrement dit: à éviter si vous avez le vertige et si les terrains est glissant. 

\subsection{Roque Faneque}
%-------------------------

Le chemin se situe dans le Parc de Tamadaba, dans le nord ouest de l'île, et descend jusqu'à un des points de vue naturels les plus impressionnants: le Roque Faneque. Celui-ci est d'ailleurs répertorié comme l'une des falaises avec le dénivelé le plus important. [source?] En réalité, l'accès au Faneque n'est possible qu'au moyen de matériel de montagne (cordes etc), et le point final de la randonnée est appelé \textit{Morro de la Lechuga}. 

On commence la randonnée près de la maison du garde forestier de Tamadaba\index{Tamadaba}. Suivre la route pendant quelques dizaines de mètre jusqu'à rencontrer une flèche indiquant \textit{Lomo del Zapatero (1256~m)}. Après cette flèche, on suivra une direction nord-ouest le long du \textit{Lomo del Faneque}. Le chemin est facile à suivre car souvent parcouru, de plus il est marqué par de nombreux petits tas de pierres.


\subsection{Cañadón del Jierro}
%---------------------

Le chemin de \textit{El Jierro} est constitue l'un des trajets qui permettent de relier la \textit{Caldera de Tirajana}\index{Caldera de Tirajana} à la \textit{cumbre}, tout comme le \textit{Camino de la Plata}\index{Camino de la Plata}. L'origine du nom viendrait de l'ancien castillan jierro ou fierro (fer), en référence au bruit de métal et à ses nombreux échos des animaux ...

\begin{anecdote}
Après avoir réalisé la descente jusque la Culata\index{La Culata}, j'avais décidé de réaliser à nouveau le parcours, mais cette fois-ci en montée. Cependant, une fois arrivé à la Culata, je ne parvenais plus à retrouver le début du chemin, cachés derrière les maisons et la végétation. J'avais alors demandé mon chemin à quelques personnes que j'avais croisé. La réponse était souvent la même: "non, ici il n'y a pas de chemin, c'est dangereux". Une autre personne m'indiqua un chemin qui était malheureusement erroné. Après plusieurs dizaines de minutes, je commençais à désespérer, jusqu'à rencontrer une vieille dame qui, après m'avoir montré une source d'eau potable, me guida dans les premières courbes du chemin. 
\end{anecdote}


\section{Les nocturnes}
%----------------------

Une expérience différente qui dévoile un nouveau visage de l'île. La faible pollution lumineuse dont bénéficient les Canaries donne un ciel étoilé impressionnant. De plus, vous pourrez observer les îles voisines comme vous ne l'aviez jamais imaginé.

\subsection{Le Pico de Osorio\label{sec:picoosorio}}

Perché à 967 mètres d'altitude, le Pico de Osorio\index{Pico de Osorio} est située à l'intersection de 3 municipalités: Teror\index{Teror}, Valleseco\index{Valleseco} et Firgas\index{Firgas}. Il est d'ailleurs appelé \textit{Pico de la Laguna} par les habitants de Valleseco et \textit{Pico el Rayo} par ceux de Firgas. Sa position privilégiée offre des vues aussi bien vers le nord et la capitale, le centre de l'île au sud et Tenerife et le Teide à l'ouest, lorsque la météo le permet. Il existe de nombreux accès au Pico, depuis chacune des 3 municipalités, le plus direct étant celui partant de la route qui longe la Laguna de Valleseco\index{Laguna de Valleseco}: depuis une courbe de cette route part sur la droite un chemin fléché, qui permet de gagner le somment sur un distance d'environ 750~m. 

La montée est raide au début puis s’adoucit au fur et à mesure que l'on s'approche du sommet. Celui-ci est marqué par une borne géodésique. La végétation abondante typique de la zone permet de profiter d'un trajet à l'ombre la plupart du temps. 

\mysidebox[title={Pico de Osorio vu depuis Valleseco.}]{%
\includegraphics[width=.6\textwidth]{PicoOsorio1.JPG}
}{%
L'altitude du sommet est similaire à celle où se trouve la couche de stratocumulus, rendant ainsi le passage par Osorio d'autant plus spectaculaire.
}

\begin{anecdote}
Il y a quelques années, près de la borne géodésique, se trouvait un livre de visite où les marcheurs ou coureurs arrivés au sommet étaient invités à laisser quelques commentaires sur leurs impressions du moment. C'était vraiment plaisant et inspirant à lire car on sentait vraiment le plaisir d'être arrivé là-haut dans ces mots. C'était aussi amusant d'essayer de retrouver les traces de notre dernière visite. Depuis lors, le livre a malheureusement disparu, dérobé par des passants peu scrupuleux.
\end{anecdote}

\mysidebox[title={Descente depuis le Pico de Osorio.}]{%
\includegraphics[width=.6\textwidth]{PicoOsorio2.JPG}
}{%
Le relief du centre de l'île apparait en arrière-plan et on peut même distinguer l'antenne militaire située sur le \textit{Pico de las Nieves}. La mer de nuage commençait à s'établir à environ 900 mètres d'altitude.
}

L'itinéraire est particulièrement recommandé en fin d'après-midi un peu avant le couché de soleil afin de profiter du panorama ouvert vers l'ouest. C'est aussi un bon terrain d’entraînement trail pour une séance de répétitions de côtés. Les différentes combinaisons de chemins permettent facilement de réaliser une route circulaire, en partant par exemple de Teror\index{Teror} et en traversant la \textit{Finca de Osorio}\index{Finca de Osorio} (\url{http://cabildo.grancanaria.com/osorio}).  

D'une surface totale de 204 hectares et une altitude entre 650~m et 967~m, la Finca de Osorio est située à l'intérieur du Parc Natural de Doramas, à cheval sur les municipalités de Teror, Valleseco et Firgas. Depuis les années 1980, elle est propriété du Cabildo de Gran Canaria. Elle est actuellement destinée à des activités en relation avec l'environnement. On y trouve une "Aula de la Nature, le Jardin Classique ainsi qu'un TODO "vivero forestier" spécialisé dans la reproduction des espèces de la laurisilva.


\subsection{La Ventana del Nublo}
%--------------------------------

Il s'agit d'un chemin qui suit une partie du \textit{Camino de Santiago}\index{Camino de Santiago} (Saint Jacques de Compostelle). On peut commencer depuis Los Llanos de la Pez où il très facile de laisser son véhicule, ou bien un peu plus loin sur le côté gauche de la route, où se trouve une aire de camping (Bailico). Le tracé ne présente aucune difficulté majeure, le chemin est large et bien marqué sur toute sa longueur. Après une courte montée jusqu'à la \textit{Degollada de los Hornos}, le chemin conduit rapidement à une esplanade au bout de laquelle se trouve la fameuse \textit{ventana} (fenêtre). La bifurcation à droite du chemin principale est indiquée par une flèche en bois.

\begin{infobox}{La route des 5 fenêtres.}
\includegraphics[width=.49\textwidth]{ojoCocodrilo.JPG} \includegraphics[width=.49\textwidth]{ojoCocodrilo.JPG}
\tcblower
La \textit{Ventana del Nublo}\index{Ventana del Nublo} n'est pas la seule formation rocheuse de ce type dans la zone. Il existe un parcours d'une dizaine de kilomètres qui passent à proximité de 5 fenêtres, parmis lesquelles l'\textit{Ojo del Cocodrilo}\index{Ojo del Cocodrilo} (l'oeil du crocodile), une point de vue unique sur la Caldera de Tirajana. 
\end{infobox}


%\input{trail.tex}

%% Trail chapter here
%---------------------

\printindex

\end{document}
